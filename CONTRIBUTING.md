# Contributing to Woggle

## Git workflow

Woggle is using a "devel branch promotion" git workflow. Subsequently **all merge requests should
be against the devel branch**.

Base your work in a new feature branch, off of devel. Ensure that your fork is up to date with
upstream to prevent conflicts, overlaps or incompatibilities.

Before any contribution, please raise an issue to ensure suitability and that nobody else is
already working on it.

## Formatting & Copyright

Woggle uses black and isort to format the code, you can easily format your code by running:
```
make format
```

Woggle also makes use of an copyright script, this automatically adds you to the copyright message on the top of each file you worked on. To run the script type:
```
make copyright
```

## Gitlab CI
Woggle has gitlab CI setup to ensure that code is properly formatted, and that the copyright messages are correct.

If the pipeline fails, make sure you executed the [Formatting & Copyright](https://gitlab.com/scoutlink/woggle/-/blob/devel/CONTRIBUTING.md#formatting-copyright) commands.

## Documentation
Documentation for sopel can be found [here](https://sopel.chat/docs/index.html).


## Environments
### Development environment

As with any python project are strongly recommended to perform your development in a python virtual
environment [virtualenv](https://virtualenv.pypa.io/en/stable/).

To install any dependencies (with virtualenv activated):

```
make install
```

To start the bot:

```
$ PYTHONPATH=. sopel start -c woggle-dev.cfg -d
```

To stop it:

```
$ PYTHONPATH=. sopel stop -c woggle-dev.cfg
```

In development the bot is using a sqlite3 DB strored in var/db.
All the logs can be found in var/log.

Modify the woggle-dev.cfg to enable additional modules but don't commit the modified verion in the
repository.

### Production environment

Each bot is running within a docker container. The Officially Unofficial™ Docker image can be used:

    $ docker run --rm -e PYTHONPATH=/home/sopel/woggle --name woggle_<instance> -d \
     -v /path/to/bot/config.cfg:/home/sopel/.sopel/default.cfg \
     -v ${PWD}/modules:/home/sopel/.sopel/modules \
     -v ${PWD}/helpers:/home/sopel/woggle/helpers \
     sopelirc/sopel

In production each bot instance has its own MySQL database.

The configs for each instance are not stored in the repository.
