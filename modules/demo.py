"""
Woggle - bot framework

  Copyright (C) 2021 Yorick Bosman <spam@gewoonyorick.nl>
  Copyright (C) 2021 Christos Triantafyllidis <christos.triantafyllidis@gmail.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sopel.module import commands

from helpers import auth


@commands("demo-account")
@auth.require_account
def demo_account(bot, trigger):
    """
    This will be executed ONLY if the user has identified to the IRC services
    """
    bot.reply(f"Trigger was from the identified user: {trigger.account}")


@commands("demo-permission")
@auth.require_permission("my_permission")
def demo_permissions(bot, trigger):
    """
    This will be executed ONLY if the user has identified to the IRC services
    and has the 'my_permission' permission
    """
    bot.reply(
        f"Trigger was from the identified user: {trigger.account} who has 'my_permission'"
    )
